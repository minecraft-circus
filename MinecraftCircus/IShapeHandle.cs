﻿using System;
using System.Windows.Forms;

namespace MinecraftCircus
{
    /// <summary>
    /// Defines contract for shape generating strategies' controls.
    /// </summary>
    public interface IShapeHandle
    {
        /// <summary>
        /// Occurs when <see cref="IShapeStarategy"/> parameters are changed.
        /// </summary>
        event EventHandler Changed;

        /// <summary>
        /// Gets this handle as a user control.
        /// </summary>
        Control AsControl { get; }
    }
}