﻿namespace MinecraftCircus
{
    /// <summary>
    /// Defines contract for getting shape generaing strategies.
    /// </summary>
    public interface IShapeFactory
    {
        /// <summary>
        /// Return shape generating strategy by name given.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        IShapeStarategy GetStartegy( string name );
    }
}