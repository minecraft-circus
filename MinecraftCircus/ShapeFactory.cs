﻿namespace MinecraftCircus
{
    public class ShapeFactory : IShapeFactory
    {
        static ShapeFactory()
        {
            Instance = new ShapeFactory();
        }

        private ShapeFactory()
        {
        }

        public static ShapeFactory Instance { get; private set; }

        public IShapeStarategy GetStartegy( string name )
        {
            return new CircusStrategy();
        }
    }
}