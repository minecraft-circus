﻿namespace MinecraftCircus
{
    partial class CircusUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelStep = new System.Windows.Forms.Label();
            this.numericUpDownRadius = new System.Windows.Forms.NumericUpDown();
            this.labelStartRadius = new System.Windows.Forms.Label();
            this.numericUpDownStep = new System.Windows.Forms.NumericUpDown();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRadius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStep)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.labelStep, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDownRadius, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelStartRadius, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDownStep, 1, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(142, 52);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // labelStep
            // 
            this.labelStep.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelStep.AutoSize = true;
            this.labelStep.Location = new System.Drawing.Point(3, 32);
            this.labelStep.Name = "labelStep";
            this.labelStep.Size = new System.Drawing.Size(29, 13);
            this.labelStep.TabIndex = 4;
            this.labelStep.Text = "Step";
            // 
            // numericUpDownRadius
            // 
            this.numericUpDownRadius.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.numericUpDownRadius.Location = new System.Drawing.Point(74, 3);
            this.numericUpDownRadius.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numericUpDownRadius.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownRadius.Name = "numericUpDownRadius";
            this.numericUpDownRadius.Size = new System.Drawing.Size(53, 20);
            this.numericUpDownRadius.TabIndex = 0;
            this.numericUpDownRadius.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownRadius.ValueChanged += new System.EventHandler(this.InvokeChanged);
            // 
            // labelStartRadius
            // 
            this.labelStartRadius.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelStartRadius.AutoSize = true;
            this.labelStartRadius.Location = new System.Drawing.Point(3, 6);
            this.labelStartRadius.Name = "labelStartRadius";
            this.labelStartRadius.Size = new System.Drawing.Size(65, 13);
            this.labelStartRadius.TabIndex = 1;
            this.labelStartRadius.Text = "Start Radius";
            // 
            // numericUpDownStep
            // 
            this.numericUpDownStep.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.numericUpDownStep.Location = new System.Drawing.Point(74, 29);
            this.numericUpDownStep.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numericUpDownStep.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDownStep.Name = "numericUpDownStep";
            this.numericUpDownStep.Size = new System.Drawing.Size(53, 20);
            this.numericUpDownStep.TabIndex = 5;
            this.numericUpDownStep.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericUpDownStep.ValueChanged += new System.EventHandler(this.InvokeChanged);
            // 
            // CircusUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "CircusUserControl";
            this.Size = new System.Drawing.Size(142, 52);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRadius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStep)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label labelStep;
        private System.Windows.Forms.NumericUpDown numericUpDownRadius;
        private System.Windows.Forms.Label labelStartRadius;
        private System.Windows.Forms.NumericUpDown numericUpDownStep;
    }
}
