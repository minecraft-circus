﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace MinecraftCircus
{
    public partial class MinecraftCircusForm : Form
    {
        private readonly int _maxradius = 50;
        private int _startRadius = 13;
        private int _squareRadius = 17;

        // Adjustment to rounding for radiuses.
        private readonly float _magic = .2f;

        private int _step = 4;
        private IShapeStarategy _strategy;

        public MinecraftCircusForm()
        {
            InitializeComponent();

            _strategy = ShapeFactory.Instance.GetStartegy( "circus" );

            var control = _strategy.Handle.AsControl;
            splitContainer1.Panel2.Controls.Add( control );
            control.Dock = DockStyle.Top;


            _strategy.Handle.Changed += new EventHandler(Handle_Changed);

            //numericUpDownRadius.Value = _startRadius;
            //numericUpDownSize.Value = _squareRadius;
            //numericUpDownStep.Value = _step;
        }

        void Handle_Changed(object sender, EventArgs e)
        {
            panel1.Invalidate();
        }

        private void panel1_Paint( object sender, PaintEventArgs e )
        {
            var pen = new Pen( Brushes.Silver );
            pen.Width = 1.5f;

            int step = Math.Min( ClientRectangle.Width, ClientRectangle.Height ) / _maxradius / 2;

            pen.Width = 1;
            for ( int i = 0; i < _maxradius * 2; i++ )
            {
                if ( i == _maxradius || i == _maxradius - 1 )
                {
                    pen.Color = Color.Black;
                }
                else
                {
                    pen.Color = Color.Silver;
                }

                e.Graphics.DrawLine( pen, new Point( 0, ( i + 1 ) * step ),
                                     new Point( ClientRectangle.Width, ( i + 1 ) * step ) );
                e.Graphics.DrawLine( pen, new Point( ( i + 1 ) * step, 0 ),
                                     new Point( ( i + 1 ) * step, ClientRectangle.Height ) );
            }

            for ( int i = 1; i < _maxradius; i++ )
            {
                DrawPixel( i, i, step, Color.Silver, e.Graphics );
                DrawPixel( i, -i, step, Color.Silver, e.Graphics );
                DrawPixel( -i, i, step, Color.Silver, e.Graphics );
                DrawPixel( -i, -i, step, Color.Silver, e.Graphics );
            }

            DrawSquare( e.Graphics, step );

            foreach ( var frame in _strategy.Frames )
            {
                foreach ( var point in frame.Points )
                {
                    DrawPixel(point.X, point.Y,step, Color.Red, e.Graphics);
                }
            }

            richTextBox1.Lines = _strategy.Descriptions.ToArray();
        }

        private void DrawSquare( Graphics graphics, int step )
        {
            int oc = ( _squareRadius - 1 ) / 2;

            for ( int i = 0; i < _squareRadius; ++i )
            {
                DrawPixel( -oc + i, -oc, step, Color.DarkGreen, graphics );
                DrawPixel( -oc, -oc + i, step, Color.DarkGreen, graphics );

                DrawPixel( -oc + i, oc, step, Color.DarkGreen, graphics );
                DrawPixel( oc, -oc + i, step, Color.DarkGreen, graphics );
            }
        }

        private void DrawPixel( int x, int y, int size, Color color, Graphics gr )
        {
            var pen = new Pen( color );
            int newX = _maxradius + x;
            int newY = _maxradius + y;
            gr.DrawLine( pen, new Point( newX * size + 1, newY * size + 1 ),
                         new Point( ( newX + 1 ) * size - 1, ( newY + 1 ) * size - 1 ) );
            gr.DrawLine( pen, new Point( ( newX + 1 ) * size - 1, newY * size + 1 ),
                         new Point( newX * size + 1, ( newY + 1 ) * size - 1 ) );
            gr.DrawRectangle( pen, newX * size + 1, newY * size + 1, size - 2, size - 2 );
        }

        private void FillCircleData()
        {
            var desciption = new List< string >();
            for ( int i = _startRadius; i <= _maxradius; i += _step )
            {
                var values = new List< int >();
                double q = i * 0.707106781 - 0.2;
                var quoter = ( int ) Math.Round( q );
                for ( int x = 0; x < quoter; x++ )
                {
                    var y = ( int ) Math.Round( Math.Sqrt( i * i - x * x ) - _magic );
                    values.Add( y );
                }

                bool odd = quoter + 1 == values.Last();

                // Get group counts
                IEnumerable< int > grouped
                    = from value in values
                      group value by value
                      into grp
                      select grp.Count();

                string res = String.Format( "R{0} {{{1}}}", i, String.Join( ", ", grouped ) );
                var rx = new Regex( @"(.+)(\d+)}$" );

                // if last number is shared between two archs - ambed in in paranthesis
                if ( odd ) res = rx.Replace( res, "$1($2)}" );

                desciption.Add( res );
            }

            richTextBox1.Lines = desciption.ToArray();
        }
    }
}