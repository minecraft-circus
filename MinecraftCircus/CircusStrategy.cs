﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;

namespace MinecraftCircus
{
    public class CircusStrategy : IShapeStarategy
    {
        private readonly CircusUserControl _control;
        private const int Maxradius = 50;
        private const double Magic = .2f;

        public CircusStrategy()
        {
            _control = new CircusUserControl();
        }

        public IEnumerable< Frame > Frames
        {
            get
            {
                int n = 0, rad;

                while ( ( rad = _control.StartRadius + ( n++ ) * _control.Step ) <= Maxradius )
                {
                    for ( int i = 0; i < rad; i++ )
                    {
                        var y = ( int ) Math.Round( Math.Sqrt( rad * rad - i * i ) - Magic );
                        yield return new Frame( new[]
                                                    {
                                                        new Point( i, y ),
                                                        new Point( y, i ),
                                                        new Point( -i, y ),
                                                        new Point( -y, i ),
                                                        new Point( i, -y ),
                                                        new Point( y, -i ),
                                                        new Point( -i, -y ),
                                                        new Point( -y, -i )
                                                    } );
                    }
                }
            }
        }

        public IShapeHandle Handle
        {
            get { return _control; }
        }

        public IEnumerable< string > Descriptions
        {
            get
            {
                var desciption = new List< string >();
                int startRadius = _control.StartRadius;
                int step = _control.Step;

                for ( int i = startRadius; i <= Maxradius; i += step )
                {
                    var values = new List< int >();
                    double q = i * 0.707106781 - 0.2;
                    var quoter = ( int ) Math.Round( q );
                    for ( int x = 0; x < quoter; x++ )
                    {
                        var y = ( int ) Math.Round( Math.Sqrt( i * i - x * x ) - Magic );
                        values.Add( y );
                    }

                    bool odd = quoter + 1 == values.Last();

                    // Get group counts
                    IEnumerable< int > grouped
                        = from value in values
                          group value by value
                          into grp
                          select grp.Count();

                    string res = String.Format( "R{0} {{{1}}}", i, String.Join( ", ", grouped ) );
                    var rx = new Regex( @"(.+)(\d+)}$" );

                    // if last number is shared between two archs - ambed in in paranthesis
                    if ( odd ) res = rx.Replace( res, "$1($2)}" );

                    desciption.Add( res );
                }

                return desciption;
            }
        }
    }
}