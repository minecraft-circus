﻿using System.Collections.Generic;

namespace MinecraftCircus
{
    /// <summary>
    /// Defines contract for shape generating strategy.
    /// </summary>
    public interface IShapeStarategy
    {
        /// <summary>
        /// Gets a list of frames (layers) for drawing.
        /// </summary>
        IEnumerable< Frame > Frames { get; }

        /// <summary>
        /// Gets control handle though which user can edit parameters.
        /// </summary>
        IShapeHandle Handle { get; }

        /// <summary>
        /// Gets shape descriptions, one per frame.
        /// </summary>
        IEnumerable< string > Descriptions { get; }
    }
}