﻿using System.Collections.Generic;
using System.Drawing;

namespace MinecraftCircus
{
    /// <summary>
    /// Defines one state of a shape.
    /// </summary>
    public sealed class Frame
    {
        private readonly IEnumerable< Point > _points;

        /// <summary>
        /// Initialize new instance of <see cref="Frame"/> with a collection of points.
        /// </summary>
        /// <param name="points"></param>
        public Frame( IEnumerable< Point > points )
        {
            _points = points;
        }

        /// <summary>
        /// Gets point for this frame.
        /// </summary>
        public IEnumerable< Point > Points
        {
            get { return _points; }
        }
    }
}