﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace MinecraftCircus
{
    public sealed partial class CircusUserControl : UserControl, IShapeHandle
    {
        public CircusUserControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Occurs when <see cref="IShapeStarategy"/> parameters are changed.
        /// </summary>
        public event EventHandler Changed;

        /// <summary>
        /// Gets this handle as a user control.
        /// </summary>
        public Control AsControl
        {
            get { return this; }
        }

        /// <summary>
        /// Gets the radius of the innermost circle.
        /// </summary>
        public int StartRadius
        {
            get { return ( int ) numericUpDownRadius.Value; }
        }

        /// <summary>
        /// Gets the difference between radiuses of two adjacent circles.
        /// </summary>
        public int Step
        {
            get { return ( int ) numericUpDownStep.Value; }
        }

        private void InvokeChanged( object sender, EventArgs e )
        {
            EventHandler temp = Interlocked.CompareExchange( ref Changed, null, null );
            if ( temp != null ) temp( this, EventArgs.Empty );
        }
    }
}